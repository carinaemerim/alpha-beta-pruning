package main.java;

import java.time.Duration;
import java.time.Instant;

public class MinimaxPlayer extends RandomPlayer {	//Confira o fonte da classe src.RandomPlayer para ver os métodos disponíveis
    public double heuristic(int[][] state) {
        return getWinner(state);	//Como o minimax consegue analisar o jogo da velha inteiro, a heurística pode ser a própria condição de vitória
    }

    /**
     * Minimax fake para primeira chamada
     *
     * @param state O estado atual do jogo.
     * @param depth A profundidade máxima de busca
     * @param prune Define se deve usar alpha-beta pruning
     *
     * @return Um double que representa a jogada.
     */
    @SuppressWarnings("SameParameterValue")
    private double minimax(int[][] state, int depth, boolean prune) { //Retorna 2 valores: o valor do estado e a melhor jogada
        double alpha = Double.NEGATIVE_INFINITY;
        double beta = Double.POSITIVE_INFINITY;
        return minimax(state, depth, currentPlayer(state), alpha, beta, prune)[1];
    }

    /**
     *
     * Função minimax
     *
     * @param state O estado atual do tabuleiro
     * @param depth A profundidade até a qual deve ser realizada a busca
     * @param player O jogador que a IA representa.
     * @param alpha Melhor pontuação encontrada para o max.
     * @param beta Melhor pontuação encontrada para o min.
     * @param prune Indica se deve ser utilizado alpha-beta pruning
     *
     * @return Um double que representa a jogada.
     */
    private double[] minimax(int[][] state, int depth, int player, double alpha, double beta, boolean prune) {

        //Inicializa o array de retorno
        double[] value = {-1, -1};

        //Se o jogo terminou ou a profundidade máxima de busca foi atingida
        //Retorne o valor da heurística
        if(isTerminal(state) || depth == 0){
            value[0] = heuristic(state) * player;
            return value;
        }
        //Pega o jogador atual de acordo com o estado
        int currentPlayer = currentPlayer(state);
        // Se o jogador atual for eu
        if(player == currentPlayer){
            value[0] = Double.NEGATIVE_INFINITY;
            // Recupera a melhor jogada para mim para cada estado filho do estado atual
            for (Integer move : validMoves(state)) {
                double[] minimax = minimax(resultingState(state, move), depth - 1, player, alpha, beta, prune);
                if (minimax[0] > value[0]) {
                    value[0] = minimax[0];
                    value[1] = move;
                }
                alpha = Math.max(value[0], alpha);
                //alpha beta pruning
                if(beta <= alpha && prune){
                    break;
                }
            }
            return value;
        }
        // Se for a vez do adversário
        value[0] = Double.POSITIVE_INFINITY;
        // Recupera a pior jogada para mim para cada estado filho do estado atual
        for (Integer move : validMoves(state)) {
            double[] minimax = minimax(resultingState(state, move), depth - 1, player, alpha, beta, prune);
            if (minimax[0] < value[0]) {
                value[0] = minimax[0];
                value[1] = move;
            }
            beta = Math.min(value[0], beta);
            //alpha beta pruning
            if(beta <= alpha && prune){
                break;
            }
        }
        return value;
    }

    public int play(int[][] state) {
        double result = minimax(state, 99, true);
        return (int) result;
    }
    public static void main(String[] args) {
        MinimaxPlayer rp = new MinimaxPlayer();
        int move = rp.play(rp.stringToState(args[0]));
        System.out.println(move);
    }
    
    // public static void main(String[] args) {
    //     MinimaxPlayer rp = new MinimaxPlayer();
    //     int[][] board = {
    //             {0,0,0},
    //             {0,0,0},
    //             {0,0,1}
    //     };
    //     Instant start = Instant.now();
    //     int move = rp.play(board);
    //     Instant end = Instant.now();
    //     System.out.println("Tempo(ms): " + Duration.between(start, end).toMillis());
    //     System.out.println("Movimento: " + move);


    // }    
}