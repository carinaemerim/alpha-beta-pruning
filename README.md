# Alpha beta pruning tic-tac-toe

## Setup

Compile the jogodavelha.c file:

`gcc -o jogodavelha jogodavelha.c`

Compile the player files

`
cd src
javac RandomPlayer.java MinimaxPlayer.java
`

Move jogodavelha to src

`cp ../jogodavelha ,/`

## Running

To run the game run:

`./jogodavelha player1 player2`

If left empty the player will be you :)

Example: human vs minimax human starts

`./jogodavelha '' minimaxplayer`
